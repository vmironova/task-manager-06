package ru.t1consulting.vmironova.tm;

import ru.t1consulting.vmironova.tm.constant.TerminalConst;
import ru.t1consulting.vmironova.tm.constant.ArgumentConst;

import java.util.Scanner;

public final class Application {

    public static void main(String[] args) {
        if (processArguments(args)) {
            exit();
            return;
        }

        System.out.println("** WELCOME TO TASK-MANAGER **");
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    public static boolean processArguments(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public static void processCommand(final String command) {
        if (command == null) {
            showErrorCommand();
            return;
        }
        switch (command) {
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.EXIT:
                exit();
            default:
                showErrorCommand();
        }
    }

    public static void processArgument(final String argument) {
        if (argument == null) {
            showErrorArgument();
            return;
        }
        switch (argument) {
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            default:
                showErrorArgument();
        }
    }

    public static void exit() {
        System.exit(0);
    }

    public static void showErrorArgument() {
        System.out.println("Error! This argument not supported...");
    }

    public static void showErrorCommand() {
        System.out.println("Error! This command not supported...");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Veronika Mironova");
        System.out.println("E-mail: vmironova@t1-consulting.ru");
        System.out.println("E-mail: mironovavg2@vtb.ru");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s - Show application version. \n", TerminalConst.VERSION, ArgumentConst.VERSION);
        System.out.printf("%s, %s - Show developer info. \n", TerminalConst.ABOUT, ArgumentConst.ABOUT);
        System.out.printf("%s, %s - Show application commands. \n", TerminalConst.HELP, ArgumentConst.HELP);
        System.out.printf("%s - Close application. \n", TerminalConst.EXIT);
    }

}