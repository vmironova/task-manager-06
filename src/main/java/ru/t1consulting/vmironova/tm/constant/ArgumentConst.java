package ru.t1consulting.vmironova.tm.constant;

public final class ArgumentConst {

    public final static String VERSION = "-v";

    public final static String ABOUT = "-a";

    public final static String HELP = "-h";

}
